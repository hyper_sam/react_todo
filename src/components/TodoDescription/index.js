import React from 'react'

const TodoDescription = (props) => {
    const { mainStore, selectedItem, inputHandler, localInput } = props;
    console.log(mainStore)

    return (
        <> 
            <div className="details-title">Details</div>
            {
                selectedItem === -1 ?
                    <div><textarea
                        type="text"
                        className="todo-description-input-box"
                        name="details"
                        value={localInput.details}
                        onChange={inputHandler}
                        placeholder="Enter details"
                    /></div> :
                    <div>{mainStore.details ? mainStore.details : ""}</div>
            }
        </>
    )
}

export default TodoDescription
