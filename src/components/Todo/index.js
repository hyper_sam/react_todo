import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faCheck, faScroll } from '@fortawesome/free-solid-svg-icons';

const Todo = (props) => {
    const {
        mainStore,
        setSelectedItem,
        deleteTodo,
        completedTodo,
    } = props;

    const handleSelect = (key) => {
        setSelectedItem(key);
    }

    const handleDelete = (key) => {
        deleteTodo(key);
        setSelectedItem(-1);
    }

    const handleDone = (key) => {
        completedTodo(key);
    }

    return (
        <>
            {
                mainStore.map((eachItem, id) =>( 
                    <div className="todo-item" key={id} style={{ textDecoration: eachItem.completed ? "line-through" : "" }}>
                        {eachItem.name}
                        <FontAwesomeIcon icon={faTrash} className="todo-actions" onClick={() => handleDelete(id)} />
                        <FontAwesomeIcon icon={faScroll} className="todo-actions" onClick={() => handleSelect(id)} />
                        <FontAwesomeIcon icon={faCheck} className="todo-actions" onClick={() => handleDone(id)} />
                    </div>
                ))
            }
        </>
    )
}

export default Todo
