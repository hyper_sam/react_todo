import React, { useState, useEffect } from 'react';
import './App.css'
import Todo from './components/Todo';
import TodoDescription from './components/TodoDescription';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';


function App() {
  const [selectedItem, setSelectedItem] = useState(0);
  const [mainStore, setMainStore] = useState([
    {
      id: 0,
      name: "Eat",
      details: "First todo of the list",
      completed: false,
    },
    {
      id: 1,
      name: "Sleep",
      details: "Second todo of the list",
      completed: true,
    }
  ]);

  const [localInput, setLocalInput] = useState({
      id: "",
      name: "",
      details: "",
      completed: false,
  })

  useEffect(() => {
    console.log(selectedItem, "clicked")
  }, [selectedItem])

  const inputHandler = (e) => {
      console.log(e.target.name, e.target.value)
      setLocalInput({
          ...localInput,
          [e.target.name]: e.target.value,
      })
  }

  const setToMainStore = (e) => {
    if (localInput.name !== "") {
      let id = mainStore.length;
      let newLocalInput = {...localInput, id: id};
      let tempStore = mainStore;
      tempStore.push(newLocalInput);
      setMainStore([...tempStore]);
      setLocalInput({
        id: "",
        name: "",
        details: "",
      });
    }
    
  }

  const deleteTodo = (id) => {
    console.log("Delete ", id);
    let newStore = mainStore;
    newStore.splice(id, 1);
    setMainStore([
      ...newStore,
    ])
  }

  const completedTodo = (id) => {
    console.log(id);
    let newStore = [...mainStore];
    newStore[id].completed = !newStore[id].completed;
    console.log(mainStore[id]);
    setMainStore(newStore);
  }

  return (
    <div className="container">
      <h1>Todos</h1>
      <div className="app">
        <div className="todo-list">
          <Todo
            mainStore={mainStore}
            setSelectedItem={setSelectedItem}
            deleteTodo={deleteTodo}
            completedTodo={completedTodo}
          />
          <div className="todo-item" onClick={() => setSelectedItem(-1)}>
            <input name="name" className="todo-input" type="text" maxLength="30" placeholder="Add a todo" value={localInput.name} onChange={inputHandler} />
            <FontAwesomeIcon icon={faPlus} onClick={setToMainStore} />
            {/* <button onClick={setToMainStore}>Add</button> */}
          </div>
        </div>
        
        <div className="todo-description">
          <TodoDescription
            mainStore={mainStore[selectedItem]}
            selectedItem={selectedItem}
            inputHandler={inputHandler}
            localInput={localInput}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
